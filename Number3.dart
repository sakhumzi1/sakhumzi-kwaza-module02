class MTNappoftheyear {
  String? name;
  String? developer;
  String? category;
  int? year;

  void MTNappdetails() {
    print("The name of the app $name");
    print("The name of the developer is $developer");
    print("Category is $category");
    print("The year won by app: $year");
    print("_______________________________");
  }

  void MTNupp() {
    var name = 'ambani-africa';
    var developer = 'Mukundi Lambani';
    var catergory = 'education';
    print("The app details in uppercase:");
    print(name.toUpperCase());
    print(developer.toUpperCase());
    print(catergory.toUpperCase());
  }
}

void main() {
  //Create object
  MTNappoftheyear mtnappoftheyear = MTNappoftheyear();

  //set variables
  mtnappoftheyear.name = 'ambani-africa';
  mtnappoftheyear.developer = 'Mukundi Lambani';
  mtnappoftheyear.category = 'education';
  mtnappoftheyear.year = 2021;
  //calling method
  mtnappoftheyear.MTNappdetails();

  MTNappoftheyear mtnuppercase = MTNappoftheyear();
  mtnuppercase.name = 'ambani-africa';
  mtnuppercase.developer = 'Mukundi Lambani';
  mtnuppercase.category = 'education';
  mtnuppercase.year = 2021;

  mtnuppercase.MTNupp();
}
